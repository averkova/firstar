using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Information : MonoBehaviour
{
    [SerializeField]
    private GameObject helpPanel;

    [SerializeField]
    private GameObject disablePanel;

    public void Inform()
    {
        if (PlayerPrefs.GetString("music") != "No")
            GetComponentInChildren<AudioSource>().GetComponentInChildren<AudioSource>()?.Play();

        helpPanel.SetActive(true);
        disablePanel.SetActive(true);

    } 
}
