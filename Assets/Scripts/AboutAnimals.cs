using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AboutAnimals : MonoBehaviour
{

    Dictionary<string, string> aboutAnimalsDictionary = new Dictionary<string, string>();

    //public DescriptionAnimalSO descriptionAnimal;

    public AnimalButtonsSO animalButtons;

    public GameObject aboutButton;

    GameObject aboutButtonClone;

    Color randomColorButton;
    
    public void ShowAbout()
    {

        List<GameObject> child = new List<GameObject>();

        for (int i = 0; i < GameObject.FindGameObjectWithTag("PanelAnimals").transform.childCount; i++)
        {
            child.Add(GameObject.FindGameObjectWithTag("PanelAnimals").transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < animalButtons.description.Count; i++)
        {
            if (!aboutAnimalsDictionary.ContainsKey(child[i].name) && !aboutAnimalsDictionary.ContainsValue(animalButtons.description[i]))
                aboutAnimalsDictionary.Add(child[i].name, animalButtons.description[i]);
            //else Debug.Log("����� ��� ����");
        }

        randomColorButton = new Color(UnityEngine.Random.Range(0f, 1f), 0.5f, 0.95f, 0.7f);/*UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), 0.5f);*/
        if (PlayerPrefs.GetString("music") != "No")
            GetComponent<AudioSource>()?.Play();

        foreach (var it in aboutAnimalsDictionary)
        {
            if (gameObject.transform.parent.gameObject.name == it.Key)
            {
                if (PlayerPrefs.GetInt("InfoOpen?") != 1)
                {
                    PlayerPrefs.SetInt("InfoOpen?", 1);
                    aboutButtonClone = Instantiate(aboutButton, new Vector3(0, 213f, 0), Quaternion.identity);
                    aboutButtonClone.transform.SetParent(GameObject.Find("Canvas").transform, false);

                    aboutButtonClone.GetComponentInChildren<TMP_Text>().text = it.Value;
                    aboutButtonClone.GetComponent<Image>().color = randomColorButton;
                    GetComponentInParent<Image>().color = randomColorButton;
                } 
                else
                {
                    Debug.Log("��� ������� ����, ���");
                }
            }

        }

    }

}
