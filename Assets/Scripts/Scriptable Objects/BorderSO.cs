using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="New Border", menuName ="SO/BorderSO")]
public class BorderSO : ScriptableObject
{
    public Sprite sprite;
}
   
