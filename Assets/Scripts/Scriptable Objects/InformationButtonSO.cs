using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New InfoButton", menuName = "SO/InformationButtonSO")]
public class InformationButtonSO : ScriptableObject
{
    public Sprite sprite;
    
}
