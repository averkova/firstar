using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName ="New Animal ButtonsSO", menuName = "SO/AnimalButtonsSO")]
public class AnimalButtonsSO : ScriptableObject
{
    public List<AssetReference> prefabsReferences;

    public List<Sprite> listAnimalPic;

    [TextArea]
    public List<string> description;

}
