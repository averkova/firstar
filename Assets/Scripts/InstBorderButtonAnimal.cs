using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstBorderButtonAnimal : MonoBehaviour
{
    public BorderSO borderSO;
   
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Image>().sprite = borderSO.sprite;
      
    }

}
