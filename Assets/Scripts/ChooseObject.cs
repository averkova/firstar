using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

public class ChooseObject : MonoBehaviour
{
    private ProgramManager programManagerScript;

    [SerializeField]
    public AssetReference choosedObject; 

    void Start()
    {
        programManagerScript = FindObjectOfType<ProgramManager>();   
    }

    public void ChooseObjectFunc()
    {
       
        programManagerScript.objectToSpawn = choosedObject;
        programManagerScript.isChooseObject = true;
       // programManagerScript.isCheck = true;
    }
}
