using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoading : MonoBehaviour
{
   
    void Start()
    {
        //  progressText = GetComponent<TextMeshPro>();
        StartCoroutine(AsyncLoad("MainScene"));
    }

    IEnumerator AsyncLoad(string sceneName)
    {
        var asyncOperation = Addressables.LoadSceneAsync(sceneName);
       

        while (!asyncOperation.IsDone)
        {  
            yield return null;
        }
    }
}
