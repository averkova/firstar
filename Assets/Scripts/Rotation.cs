using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rotation : MonoBehaviour
{
    private ProgramManager programManagerScript;

    //private Button button;

    void Start()
    {
        //button = GetComponent<Button>();
        programManagerScript = FindObjectOfType<ProgramManager>();
    }

   public void RotationFunc()
    {
        if (PlayerPrefs.GetString("music") != "No")
            GetComponentInChildren<AudioSource>().Play();


        Color color = new Color(255, 255, 255, 0.39f);
        if (programManagerScript.isRotation)
        {
            programManagerScript.isRotation = false;
           
            GetComponent<Image>().color = color;

        }
        else
        { 
            programManagerScript.isRotation = true;
          
            GetComponentInParent<Image>().color = Color.green;
        }
    }
}
