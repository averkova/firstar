using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ProgramManager : MonoBehaviour
{
    [Header("Put PlaneMarker here ")]

    //[SerializeField]
    //GameObject planeMarkerPrefab;

    [SerializeField]
    AssetReferenceGameObject planeMarkerPrefab;

    //public GameObject objectToSpawn;


    [Header("object to spawn ")]
    public AssetReference objectToSpawn;

    [SerializeField]
    private Camera ARCamera;

    [SerializeField]
    float perspectiveZoomSpeed;

    [SerializeField]
    GameObject cameraBackground;

    [SerializeField]
    GameObject textSetObjectHint;

    [SerializeField]
    GameObject planeHint;

    ARRaycastManager ARRaycastManagerScript;

    Vector2 touchPosition;

    List<ARRaycastHit> hits = new List<ARRaycastHit>();

    public bool isChooseObject = false;

    GameObject selectedObject;

    public bool isRotation = false; // ������������ ������ rotation

    Quaternion yRotation;

    bool _IsEscape = false;

    AsyncOperationHandle<GameObject> addressableGameObject;

    void Start()
    {
        ARRaycastManagerScript = FindObjectOfType<ARRaycastManager>();

         addressableGameObject = Addressables.InstantiateAsync(planeMarkerPrefab, new Vector3(-1,-1,-1), Quaternion.identity);

        StartCoroutine(BackgroundBeforeCameraStart());
    }

    IEnumerator BackgroundBeforeCameraStart()
    {
        cameraBackground.SetActive(true);
        // Debug.Log("SetActive(true)");
        yield return new WaitForSeconds(3f);
        cameraBackground.SetActive(false);
        // Debug.Log("SetActive(false)");
    }

    void Update()
    {
        if (addressableGameObject.IsDone)
            addressableGameObject.Result.SetActive(false);  
        
       // var planeManager = FindObjectOfType<ARPlaneManager>();

        var planesCount = FindObjectOfType<ARPlaneManager>().trackables.count;

        if (planesCount <= 0)
            planeHint.SetActive(true);
        else
            planeHint.SetActive(false);

        if (isChooseObject)
        {
            ShowMarker();
            
            if (Physics.Raycast(ARCamera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
            { 
                if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began && hit.transform.name != "PanelAnimals"/*&& PlayerPrefs.GetString("AnimalInst?") == "No"*/)
                {
                    Debug.Log("������ ������");
                    //PlayerPrefs.SetString("AnimalInst?", "Yes");  
                    SetObject();
                   // Debug.Log(PlayerPrefs.GetString("AnimalInst?"));
                }
                else { Debug.Log("������ � ������"); }
            };
        }

        MoveAndRotateObject();


        ScaleObject();


        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (_IsEscape)
                Application.Quit();     
            else
            {
                _IsEscape = true;
                if (!IsInvoking("DisableDoubleClick"))
                    Invoke("DisableDoubleClick", 0.3f);
            }
        } 
    }

    void ShowMarker()
    {
        ARRaycastManagerScript.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.Planes);

        if (hits.Count > 0)
        {
            addressableGameObject.Result.transform.position = hits[0].pose.position;
            addressableGameObject.Result.SetActive(true);
            textSetObjectHint.SetActive(true);
        }
      
    }

    void SetObject()
    {
        Addressables.InstantiateAsync(objectToSpawn, hits[0].pose.position, Quaternion.identity);
        isChooseObject = false;
        addressableGameObject.Result.SetActive(false);
        textSetObjectHint.SetActive(false);
    }

    void MoveAndRotateObject()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            touchPosition = touch.position;

            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = ARCamera.ScreenPointToRay(touchPosition);
                if (Physics.Raycast(ray, out RaycastHit raycastHit))
                    if (raycastHit.collider.CompareTag("UnSelected"))
                        raycastHit.collider.gameObject.tag = "Selected";
            }

            if (GameObject.FindWithTag("Selected") != null)
            {        
                selectedObject = GameObject.FindWithTag("Selected");            

                if (touch.phase == TouchPhase.Moved && Input.touchCount == 1)
                {
                    if (isRotation)
                    {
                        yRotation = Quaternion.Euler(0f, -touch.deltaPosition.x * 0.1f, 0f);
                        selectedObject.transform.rotation = yRotation * selectedObject.transform.rotation;
                    }
                    else
                    {
                        ARRaycastManagerScript.Raycast(touchPosition, hits, TrackableType.Planes);
                       // Debug.Log($"hits.count: {hits.Count}");

                        if (hits.Count != 0)
                        {
                           // Debug.Log($"selectedObject.transform.position = {hits[0].pose.position}");
                            selectedObject.transform.position = hits[0].pose.position;
                        }
                    }
                }

                if (touch.phase == TouchPhase.Ended)
                    if (selectedObject.CompareTag("Selected"))
                        selectedObject.tag = "UnSelected";
            }
        }
    }

    void ScaleObject()
    {
        if (Input.touchCount == 2 && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId) && selectedObject !=null)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            float scale = deltaMagnitudeDiff * perspectiveZoomSpeed * Time.deltaTime;


            if (selectedObject.transform.localScale.x > 0 && selectedObject.transform.localScale.y > 0 && selectedObject.transform.localScale.z > 0)
            {
                selectedObject.transform.localScale += new Vector3(-scale, -scale, -scale);
            }

            if (selectedObject.transform.localScale.x < 0.2 && selectedObject.transform.localScale.y < 0.2 && selectedObject.transform.localScale.z < 0.2)
            {
                selectedObject.transform.localScale += new Vector3(scale, scale, scale);
            }

            if (selectedObject.transform.localScale.x > 3 && selectedObject.transform.localScale.y > 3 && selectedObject.transform.localScale.z > 3)
            {
                selectedObject.transform.localScale += new Vector3(scale, scale, scale);
            }
        }
    }
}