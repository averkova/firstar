using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public GameObject prefabOfButton;
   
    //public AnimalPicSO animalPic;
    //public PrefabsSO prefabs;

    public AnimalButtonsSO animalButtons;

    void Start()
    {  
        SpawnEntities();
    }

    void SpawnEntities()
    {
       
       // Debug.Log(prefabs.prefabsReferences.Count);
       
        for (int i = 0; i < animalButtons.prefabsReferences.Count; i++)
        {
           
            GameObject currentEntity = Instantiate(prefabOfButton, transform.position, Quaternion.identity);
 
            currentEntity.transform.Find("ButtonAnimal").GetComponent<Image>().sprite = animalButtons.listAnimalPic[i];
        
            currentEntity.transform.SetParent(GameObject.FindGameObjectWithTag("PanelAnimals").transform, false);

            //?
            currentEntity.name = animalButtons.listAnimalPic[i].name.Replace("Image", "");  
            
            currentEntity.gameObject.GetComponent<ChooseObject>().choosedObject = animalButtons.prefabsReferences[i];       
        }
    }
}
