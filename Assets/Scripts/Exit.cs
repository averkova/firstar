using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    
    public void CloseApp()
    {
        if (PlayerPrefs.GetString("music") != "No")
            GetComponentInChildren<AudioSource>().GetComponentInChildren<AudioSource>()?.Play();

        Application.Quit();
    }

    public void CloseWindow()
    {
        if (PlayerPrefs.GetString("music") != "No")
            GetComponent<AudioSource>()?.Play();

        if (gameObject != null)
        {
            Destroy(gameObject);
            PlayerPrefs.SetInt("InfoOpen?", 0);

        }

        gameObject.SetActive(false);
    }
}
