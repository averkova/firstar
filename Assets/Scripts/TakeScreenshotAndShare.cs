using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TakeScreenshotAndShare : MonoBehaviour
{

	[SerializeField]
	private GameObject UIPanelBottom;
	[SerializeField]
	private GameObject UIPanelTop;

	[SerializeField]
	private Color colorBeforeScreenshot = new Color(255,255,255,0); 

	[SerializeField]
	private Color colorAfterScreenshot; 
	

    private void Start()
    {
		colorAfterScreenshot = UIPanelTop.GetComponent<Image>().color;
		//Debug.Log(colorAfterScreenshot.a);

	}

    public void Share()
    {

		if (PlayerPrefs.GetString("music") != "No")
			GetComponentInChildren<AudioSource>().GetComponentInChildren<AudioSource>().Play();

		//hide UI 
		UIPanelBottom.GetComponent<Image>().color = colorBeforeScreenshot;//new Color(255, 255, 255, 0);
		UIPanelTop.GetComponent<Image>().color = colorBeforeScreenshot; //new Color(255, 255, 255, 0);

		StartCoroutine(TakeSSAndShare());
		
    }

	private IEnumerator TakeSSAndShare()
	{
		
		yield return new WaitForEndOfFrame();

		Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		texture.Apply();

		string filePath = Path.Combine(Application.temporaryCachePath, "shared_img.png");
		File.WriteAllBytes(filePath, texture.EncodeToPNG());

		// To avoid memory leaks
		Destroy(texture);

		//show UI
		UIPanelBottom.GetComponent<Image>().color = colorAfterScreenshot; //new Color(255, 255, 255, 5);
		UIPanelTop.GetComponent<Image>().color = colorAfterScreenshot; //new Color(255, 255, 255, 5);

		new NativeShare().AddFile(filePath)
			.SetSubject("Subject goes here").SetText("��� ������ �������� � ���������!")
			.SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
			.Share();
		
	}
}
