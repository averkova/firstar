using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Help : MonoBehaviour
{
    private List<string> textsHelp = new List<string>();

    [SerializeField]
    private GameObject[] buttons;

    [SerializeField]
    private GameObject panelDisable;

    private Color helpColor;
    private Color defaultColor;

    private int v = 0;

    private void Start()
    {
        textsHelp.Add("������ � ��������� � ����������.\n��� ��������� ������ � ������������ �� ���������� ������� �� ������ � ��������� � ����� ��������� �����.");
        textsHelp.Add("��� ������������ ���� ������ ����� ������� ������� �� �����.\n\n<i>����������� � ��������������� �������� �������������� � ������� ������.<i>");
        textsHelp.Add("����������� ������� �������� ��������� ����� � ���������� �� � ������.");
        textsHelp.Add("��������/��������� ����");
        textsHelp.Add("����� �� ����������");
      //  panelDisable.SetActive(true);
      
        helpColor = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), 0.7f);
        defaultColor = buttons[v].GetComponent<Image>().color;
        var a = buttons[v].GetComponent<Image>().color.a;
        defaultColor.a = 0;
     
        GetComponent<Image>().color = defaultColor;

    }

    private void Update()
    {
        
        if (v == 0)
        {
            GetComponentInChildren<TMP_Text>().text = textsHelp[v];
          //  buttons[v].GetComponent<Image>().color.a = 0.3921569;
            buttons[v].GetComponent<Image>().color = helpColor;
            gameObject.GetComponent<Image>().color = helpColor;

        }
    }

    public void ChangeHint()
    {
        helpColor = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), 0.7f);

        if (PlayerPrefs.GetString("music") != "No")
            GetComponentInChildren<AudioSource>().GetComponentInChildren<AudioSource>()?.Play();

        StartCoroutine(ChangeHintCoroutine());
        v++;
        if (v == buttons.Length)
        {
           // Debug.Log("3 "+v);
            Debug.Log(buttons.Length);
            buttons[v - 1].GetComponent<Image>().color = defaultColor;
            v = 0;
    
            gameObject.SetActive(false);
            panelDisable.SetActive(false);
        };
        
    }

    IEnumerator ChangeHintCoroutine()
    {  
        while (v <= buttons.Length)
        {     
            GetComponentInChildren<TMP_Text>().text = textsHelp[v];
            buttons[v].GetComponent<Image>().color = helpColor;
            gameObject.GetComponent<Image>().color = helpColor;

            // Debug.Log(v);
            if (v > 0)
            {
                if (buttons[v - 1].GetComponent<Image>().color != defaultColor)
                    buttons[v - 1].GetComponent<Image>().color = defaultColor;
            }

            yield return null;     
        } 
    }
}
