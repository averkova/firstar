using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstInfoButton : MonoBehaviour
{
    public InformationButtonSO infoButtonSO;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Image>().sprite = infoButtonSO.sprite;

    }
}
